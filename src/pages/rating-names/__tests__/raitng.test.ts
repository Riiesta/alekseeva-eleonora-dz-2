import { test, expect } from '@playwright/test';
import { RatingPage } from '../__page-object__/rating.page';

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
}) => {
  await page.route(
    'https://meowle.fintech-qa.ru/api/likes/cats/rating',
    route => {
      route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify({
          message: 'Server error',
        }),
      });
    }
  );

  await page.goto('https://meowle.fintech-qa.ru/rating');

  await expect(page.locator('text=Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается по убыванию', async ({ page }) => {
  await page.route(
    'https://meowle.fintech-qa.ru/api/likes/cats/rating',
    route => {
      route.fulfill({
        status: 200,
        contentType: 'application/json',
        body: JSON.stringify({
          likes: [
            { id: 16212, name: 'М-пннаа', likes: 558 },
            { id: 14016, name: 'Здчёедлжьшздчёедлжьш', likes: 124 },
            { id: 16545, name: 'Сдашабагала', likes: 104 },
            { id: 16852, name: 'Гилтикус', likes: 86 },
            { id: 16947, name: 'Джелу', likes: 82 },
            { id: 15480, name: 'Терех', likes: 78 },
            { id: 15224, name: 'Бидоня', likes: 76 },
            { id: 13379, name: 'Елюуузшёою', likes: 63 },
            { id: 18708, name: 'Ратататата', likes: 45 },
            { id: 17618, name: 'Иоадоюалца', likes: 40 },
          ],
          dislikes: [
            { id: 15393, name: 'Некий', dislikes: 51 },
            { id: 15678, name: 'Оаапп', dislikes: 51 },
            { id: 17034, name: 'Быжълтгнмк', dislikes: 50 },
            { id: 14016, name: 'Здчёедлжьшздчёедлжьш', dislikes: 50 },
            { id: 13379, name: 'Елюуузшёою', dislikes: 40 },
            { id: 13511, name: 'Щнпмяшочтз', dislikes: 36 },
            { id: 15313, name: 'Валерий', dislikes: 35 },
            { id: 16961, name: 'Ууууууууу', dislikes: 35 },
            { id: 15040, name: 'Жжж', dislikes: 26 },
            { id: 13739, name: 'Майонез', dislikes: 22 },
          ],
        }),
      });
    }
  );

  const ratingPage = new RatingPage(page);
  await ratingPage.goto();
  await ratingPage.verifySortedByLikes();
});
