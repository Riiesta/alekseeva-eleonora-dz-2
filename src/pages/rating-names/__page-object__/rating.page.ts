import { Page, expect } from '@playwright/test';

export class RatingPage {
  private page: Page;
  private likeRatingItems: string;

  constructor(page: Page) {
    this.page = page;
    this.likeRatingItems = '[data-testid="like-rating"] .item-count';
  }

  async goto() {
    await this.page.goto('https://meowle.fintech-qa.ru/rating');
  }

  async getLikes() {
    const likeElements = await this.page.$$(this.likeRatingItems);
    const likes = await Promise.all(
      likeElements.map(async (element: any) => {
        const className = await element.getAttribute('class');
        if (className?.includes('has-text-success')) {
          return parseInt(await element.textContent(), 10);
        }
        return null;
      })
    );
    return likes.filter(like => like !== null);
  }

  async verifySortedByLikes() {
    const likes = await this.getLikes();
    for (let i = 0; i < likes.length - 1; i++) {
      expect(likes[i]).toBeGreaterThanOrEqual(likes[i + 1]);
    }
  }
}




